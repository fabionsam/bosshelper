unit U_Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.AppEvnts, Vcl.Menus,
  System.Generics.Collections, Vcl.StdCtrls, U_Boss, U_Funcoes, U_Timer, DateUtils,
  Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit1: TEdit;
    Button3: TButton;
    Button4: TButton;
    TrackBar1: TTrackBar;
    Edit3: TEdit;
    Edit4: TEdit;
    TrackBar2: TTrackBar;
    TrayIcon1: TTrayIcon;
    Timer1: TTimer;
    FontDialog1: TFontDialog;
    FontDialog2: TFontDialog;
    PopupMenu1: TPopupMenu;
    MostrarRelgiodeBoss1: TMenuItem;
    BloquearClicknoRelgio1: TMenuItem;
    MostrarNotificao1: TMenuItem;
    Configuraes1: TMenuItem;
    Fechar1: TMenuItem;
    Button2: TButton;
    Button1: TButton;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    Edit2: TEdit;
    Label7: TLabel;
    Label13: TLabel;
    Edit5: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    Edit6: TEdit;
    Label16: TLabel;
    Label17: TLabel;
    Edit7: TEdit;
    Label18: TLabel;
    Label21: TLabel;
    TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    Label22: TLabel;
    MostrarRelgiodeComida1: TMenuItem;
    Label23: TLabel;
    Button5: TButton;
    FontDialog3: TFontDialog;
    RadioGroup1: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure MostrarNotificao1Click(Sender: TObject);
    procedure MostrarRelgiodeBoss1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Configuraes1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure Edit3Exit(Sender: TObject);
    procedure Edit4Exit(Sender: TObject);
    procedure BloquearClicknoRelgio1Click(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure MostrarRelgiodeComida1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  BossTime: TBosses;
  LastNowTime: Integer;
  LastNotification: TTime;
  LastHorario : TDateTime;
  LastDay : TDate;
  LastName: String;
  Notificou: Boolean;
  ImagesArray: Array[0..6] of string = ('ku.png', 'kz.png', 'ka.png', 'mk.png', 'nv.png', 'qt.png', 'of.png');

implementation

uses
  U_FoodTimer;

{$R *.dfm}

procedure TForm1.BloquearClicknoRelgio1Click(Sender: TObject);
begin
  BloquearClicknoRelgio1.Checked := not BloquearClicknoRelgio1.Checked;
  Configuracao.BloquearClick := BloquearClicknoRelgio1.Checked;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  SalvarConfiguracao('TempoEspera', Edit1.Text);
  SalvarConfiguracao('Antecedencia', Edit2.Text);
  SalvarConfiguracao('Intervalo', Edit3.Text);
  SalvarConfiguracao('Piscar', Edit4.Text);
  SalvarConfiguracao('Blend', IntToStr(TrackBar1.Position));
  SalvarConfiguracao('Tamanho', IntToStr(TrackBar2.Position));

  //SalvarConfiguracao('TempoEsperaFood', Edit5.Text);
  SalvarConfiguracao('AntecedenciaFood', Edit5.Text);
  SalvarConfiguracao('IntervaloFood', Edit6.Text);
  SalvarConfiguracao('PiscarFood', Edit7.Text);
  SalvarConfiguracao('BlendFood', IntToStr(TrackBar3.Position));
  SalvarConfiguracao('TamanhoFood', IntToStr(TrackBar4.Position));
  SalvarConfiguracao('AudioFood', IntToStr(RadioGroup1.ItemIndex));

  SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBoss.font', fmCreate), FontDialog1.Font);
  SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBossTime.font', fmCreate), FontDialog2.Font);
  SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontFoodTime.font', fmCreate), FontDialog3.Font);

  Configuracao.FonteBoss := FontDialog1.Font;
  Configuracao.FonteBossTime := FontDialog2.Font;
  Configuracao.FonteFoodTime := FontDialog3.Font;
  Configuracao.TempoEspera := StrToInt(Edit1.Text);
  Configuracao.Antecedencia := StrToInt(Edit2.Text);
  Configuracao.Intervalo := StrToInt(Edit3.Text);
  Configuracao.Piscar := StrToInt(Edit4.Text);
  Configuracao.Blend := TrackBar1.Position;
  Configuracao.Tamanho := TrackBar2.Position;
  Configuracao.TamanhoFood := TrackBar4.Position;
  Configuracao.BlendFood := TrackBar3.Position;
  //Configuracao.TempoEsperaFood := StrToInt(Edit8.Text);
  Configuracao.AntecedenciaFood := StrToInt(Edit5.Text);
  Configuracao.IntervaloFood := StrToInt(Edit6.Text);
  Configuracao.PiscarFood := StrToInt(Edit7.Text);
  Configuracao.AudioFood := RadioGroup1.ItemIndex;
  Close;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  F_Timer.ResizeAll(Configuracao.Tamanho);
  Close;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  if not FontDialog1.Execute() then
    exit;

  if (FontDialog1.Font.Color = clBlack) then
  begin
    ShowMessage('Fonte n�o pode ser da cor preta!');
    FontDialog1.Font.Color := F_Timer.Label1.Font.Color;
  end;

  F_Timer.Label1.Font := FontDialog1.Font;
  F_Timer.Label1.Repaint;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if not FontDialog2.Execute() then
    exit;

  if (FontDialog2.Font.Color = clBlack) then
  begin
    ShowMessage('Fonte n�o pode ser da cor preta!');
    FontDialog2.Font.Color := F_Timer.Label2.Font.Color;
  end;

  F_Timer.Label2.Font := FontDialog2.Font;
  F_Timer.Label2.Repaint;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  if not FontDialog3.Execute() then
    exit;

  if (FontDialog3.Font.Color = clBlack) then
  begin
    ShowMessage('Fonte n�o pode ser da cor preta!');
    FontDialog3.Font.Color := F_FoodTimer.Label1.Font.Color;
  end;

  F_FoodTimer.Label1.Font := FontDialog3.Font;
  F_FoodTimer.Label1.Font.Size := 28;
  F_FoodTimer.Label1.Repaint;
end;

procedure TForm1.Configuraes1Click(Sender: TObject);
begin
  Self.Show;
  Self.WindowState := wsNormal;
end;

procedure TForm1.Edit1Exit(Sender: TObject);
var
  Tempo : Integer;
begin
  try
    Tempo := StrToInt(Edit1.Text);
    if (Tempo > 5) then
    begin
      ShowMessage('M�ximo de 5 minutos!');
      Edit1.Text := '5';
    end;

    if (Tempo < 0) then
    begin
      ShowMessage('Somente valor maior que 0!');
      Edit1.Text := '0';
    end;
  except
    ShowMessage('Apenas n�meros!');
    Edit1.Text := '0';
  end;
end;

procedure TForm1.Edit2Exit(Sender: TObject);
var
  Tempo : Integer;
begin
  try
    Tempo := StrToInt(Edit2.Text);
    if (Tempo > 60) then
    begin
      ShowMessage('M�ximo de 60 minutos!');
      Edit2.Text := '60';
    end;

    if (Tempo < 0) then
    begin
      ShowMessage('Somente valor maior ou igual a 0!');
      Edit2.Text := '0';
    end;
  except
    ShowMessage('Apenas n�meros!');
    Edit2.Text := '0';
  end;
end;

procedure TForm1.Edit3Exit(Sender: TObject);
var
  Tempo, Tempo2 : Integer;
begin
  try
    Tempo := StrToInt(Edit3.Text);
    Tempo2 := StrToInt(Edit2.Text);
    if (Tempo > Tempo2) then
    begin
      ShowMessage('N�o pode ser maior que o tempo de notifica��o!');
      Edit3.Text := IntToStr(Tempo2 - 1);
    end;

    if (Tempo < 0) then
    begin
      ShowMessage('Somente valor maior ou igual a 0!');
      Edit3.Text := '0';
    end;
  except
    ShowMessage('Apenas n�meros!');
    Edit3.Text := '0';
  end;
end;

procedure TForm1.Edit4Exit(Sender: TObject);
var
  Tempo : Integer;
begin
  try
    Tempo := StrToInt(Edit4.Text);
    if (Tempo > 20) then
    begin
      ShowMessage('M�ximo de 20 minutos!');
      Edit4.Text := '20';
    end;

    if (Tempo < 0) then
    begin
      ShowMessage('Somente valor maior ou igual a 0!');
      Edit4.Text := '0';
    end;
  except
    ShowMessage('Apenas n�meros!');
    Edit4.Text := '0';
  end;
end;

procedure TForm1.Fechar1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  F_Timer.AlphaBlendValue := Configuracao.Blend;
  F_Timer.Label1.Font := Configuracao.FonteBoss;
  F_Timer.Label2.Font := Configuracao.FonteBossTime;
  F_Timer.Label1.Repaint;
  F_Timer.Label2.Repaint;

  F_Timer.ResizeAll(Configuracao.Tamanho);

  Self.Hide;
  Action := caNone;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i : BYTE;
begin
  LastDay := 0;

  for i := 0 to 5 do
  begin
    if not FileExists(ExtractFilePath(Application.ExeName) + '\images\' + ImagesArray[i]) then
    begin
      ShowMessage('Arquivo de imagem ' + ImagesArray[i] + ' n�o encontrado!');
      Application.Terminate;
    end;
  end;

  LastNowTime := -1;
  BossTime := LerArquivoBoss();
  LerConfiguracoes();

  MostrarNotificao1.Checked := Configuracao.ExibeNotificacao;
  MostrarRelgiodeBoss1.Checked := Configuracao.ExibeTimer;
  MostrarRelgiodeComida1.Checked := Configuracao.ExibeTimerFood;

  TrayIcon1.Visible := True;
  TrayIcon1.Animate := True;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  TrackBar1.Position := Configuracao.Blend;
  TrackBar2.Position := Configuracao.Tamanho;
  FontDialog1.Font := Configuracao.FonteBoss;
  FontDialog2.Font := Configuracao.FonteBossTime;
  Edit1.Text := IntToStr(Configuracao.TempoEspera);
  Edit2.Text := IntToStr(Configuracao.Antecedencia);
  Edit3.Text := IntToStr(Configuracao.Intervalo);
  Edit4.Text := IntToStr(Configuracao.Piscar);

  TrackBar3.Position := Configuracao.BlendFood;
  TrackBar4.Position := Configuracao.TamanhoFood;
  FontDialog3.Font := Configuracao.FonteFoodTime;
  Edit5.Text := IntToStr(Configuracao.AntecedenciaFood);
  Edit6.Text := IntToStr(Configuracao.IntervaloFood);
  Edit7.Text := IntToStr(Configuracao.PiscarFood);
end;

procedure TForm1.MostrarNotificao1Click(Sender: TObject);
begin
  MostrarNotificao1.Checked := not MostrarNotificao1.Checked;
  if (MostrarNotificao1.Checked) then
  begin
    TrayIcon1.BalloonHint := 'Notifica��o ativada!';
    TrayIcon1.ShowBalloonHint;
  end;
  SalvarConfiguracao('ExibeNotificacao', BoolToStr(MostrarNotificao1.Checked));
end;

procedure TForm1.MostrarRelgiodeBoss1Click(Sender: TObject);
begin
  MostrarRelgiodeBoss1.Checked := not MostrarRelgiodeBoss1.Checked;
  SalvarConfiguracao('ExibeTimer', BoolToStr(MostrarRelgiodeBoss1.Checked));
end;

procedure TForm1.MostrarRelgiodeComida1Click(Sender: TObject);
begin
  MostrarRelgiodeComida1.Checked := not MostrarRelgiodeComida1.Checked;
  SalvarConfiguracao('ExibeTimerFood', BoolToStr(MostrarRelgiodeComida1.Checked));
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  NowTime: Integer;
  Extract : TStringList;
  Resto : Integer;
  X, Y : Integer;
begin

  if (LastDay = 0) or (LastDay <> Date) then
    LastDay := Date;

  if (Configuracao.MouseIn) then
  begin
    X := Mouse.CursorPos.X;
    Y := Mouse.CursorPos.Y;
    if ((Y < F_Timer.Top) or (Y > (F_Timer.Top + Trunc(F_Timer.Height * (Configuracao.Tamanho/100))))) or
       ((X < F_Timer.Left) or (X > (F_Timer.Left + Trunc(F_Timer.Width * (Configuracao.Tamanho/100)))))
    then
    begin
      F_Timer.Show;
      Configuracao.MouseIn := False;
    end;
    exit;
  end;

  if (Configuracao.MouseInFood) then
  begin
    X := Mouse.CursorPos.X;
    Y := Mouse.CursorPos.Y;
    if ((Y < F_FoodTimer.Top) or (Y > (F_FoodTimer.Top + Trunc(F_FoodTimer.Height * (Configuracao.Tamanho/100))))) or
       ((X < F_FoodTimer.Left) or (X > (F_FoodTimer.Left + Trunc(F_FoodTimer.Width * (Configuracao.Tamanho/100)))))
    then
    begin
      F_FoodTimer.Show;
      Configuracao.MouseInFood := False;
    end;
    exit;
  end;

  if (MostrarRelgiodeBoss1.Checked) and (not F_Timer.Visible) then
    F_Timer.Show
  else
  if (not MostrarRelgiodeBoss1.Checked) then
    F_Timer.Hide;

  if (MostrarRelgiodeComida1.Checked) and (not F_FoodTimer.Visible) then
    F_FoodTimer.Show
  else
  if (not MostrarRelgiodeComida1.Checked) then
    F_FoodTimer.Hide;

  NowTime := GetNowTime;
  if (LastNowTime <> NowTime) then
  begin
    LastNowTime := NowTime;
    Notificou := false;
  end
  else
  begin
    Resto := MinutesBetween(Now, LastHorario);
    if (MostrarNotificao1.Checked) and (not Notificou) and (Resto <= Configuracao.Antecedencia) then
    begin
      Notificou := True;
      LastNotification := Time;
      TrayIcon1.BalloonHint := 'Faltam ' + IntToStr(Resto) + ' minutos para o(s) boss ' + LastName;
      TrayIcon1.ShowBalloonHint;
    end;

    if (Configuracao.Intervalo > 0) and (MostrarNotificao1.Checked) and (Notificou) and (MinutesBetween(Time, LastNotification) >= Configuracao.Intervalo) then
    begin
      LastNotification := Time;
      TrayIcon1.BalloonHint := 'Faltam ' + IntToStr(Resto) + ' minutos para o(s) boss ' + LastName;
      TrayIcon1.ShowBalloonHint;
    end;

    if (MostrarRelgiodeBoss1.Checked) then
    begin
      if (Configuracao.Piscar > 0) and (Resto <= Configuracao.Piscar) then
      begin
        if (F_Timer.Label2.Visible) then
          F_Timer.Label2.Hide
        else
          F_Timer.Label2.Show;
      end
      else
        if (not F_Timer.Label2.Visible) then
          F_Timer.Label2.Show;

      F_Timer.Label2.Caption := FormatDateTime('hh:mm:ss', LastHorario - Now);
    end;
    Exit;
  end;

  with BossTime[GetNowDayWeek - 1][NowTime] do
  begin
    if (TimeToStr(Horario) = '02:00:00') and (LastDay = Date) then
      LastHorario := IncDay(Date, 1) + Horario
    else
      LastHorario := Date + Horario;

    LastName := Nome;

    F_Timer.Label1.Caption := Nome + '(' + TimeToStr(Horario) + ')';
    F_Timer.Label2.Caption := FormatDateTime('hh:mm:ss', LastHorario - Now);

    if Nome.Contains('|') then
    begin
      Extract := TStringList.Create();
      Extract.Delimiter := '|';
      Extract.DelimitedText := ImageName;

      F_Timer.Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + '\images\' + Extract[0] + '.png');
      F_Timer.Image2.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + '\images\' + Extract[1] + '.png');

      F_Timer.Panel1.Left := Trunc(88 * (Configuracao.Tamanho/100));
      F_Timer.Panel2.Left := Trunc(175 * (Configuracao.Tamanho/100));
      F_Timer.Panel2.Show;
    end
    else
    begin
      F_Timer.Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + '\images\' + ImageName + '.png');
      F_Timer.Panel1.Left := Trunc(136 * (Configuracao.Tamanho/100));
      F_Timer.Panel2.Hide;
    end;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  if (F_Timer.Visible) then
    F_Timer.AlphaBlendValue := TrackBar1.Position;
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  F_Timer.ResizeAll(TrackBar2.Position);
end;

procedure TForm1.TrackBar3Change(Sender: TObject);
begin
  if (F_FoodTimer.Visible) then
    F_FoodTimer.AlphaBlendValue := TrackBar3.Position;
end;

end.
