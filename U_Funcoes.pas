unit U_Funcoes;

interface

uses
  System.Generics.Collections, U_Boss, Vcl.Graphics, System.Classes, PSAPI, TlHelp32;

type
  TBosses = Array of TList<TBoss>;

type
  TMinhaFonte = record
    Nome : array [1..50] of Char;
    CharSet: TFontCharSet;
    Color: TColor;
    Size: Integer;
    Style: TFontStyles;
  end;

type
  TConfiguracao = record
    TempoEspera: Integer;
    Antecedencia: Integer;
    Intervalo: Integer;
    Piscar: Integer;
    ExibeTimer: Boolean;
    ExibeNotificacao: Boolean;
    FonteBoss: TFont;
    FonteBossTime: TFont;
    Blend: Byte;
    BloquearClick: Boolean;
    MouseIn : Boolean;
    Tamanho: Integer;

    TempoEsperaFood: Integer;
    AntecedenciaFood: Integer;
    IntervaloFood: Integer;
    PiscarFood: Integer;
    ExibeTimerFood: Boolean;
    TamanhoFood: Integer;
    BlendFood: Byte;
    FonteFoodTime: TFont;
    AudioFood : Byte;
    MouseInFood : Boolean;
  end;

function LerArquivoBoss(): TBosses;
procedure LerConfiguracoes();
procedure SaveFontToStream(AStream: TFileStream; AFont: TFont);
procedure LoadFontFromStream(AStream: TFileStream; var AFont: TFont);
procedure SalvarConfiguracao(Configuracao, Valor: String);
function GetTime(Indice: Integer): TTime;
function GetNowDayWeek(): Integer;
function GetNowTime(): Integer;
function ReturnTimeInternet(const Servidor: string): TDateTime;

var
  Configuracao: TConfiguracao;

implementation

uses
  Vcl.Forms, System.SysUtils, Vcl.Dialogs,
  IdComponent, IdTCPConnection, IdTCPClient, IdSNTP, IniFiles,
  IdBaseComponent, IdUDPClient, IdUDPBase, System.DateUtils, Winapi.Windows,
  Winapi.Messages;

function LerArquivoBoss(): TBosses;
var
  Arquivo, Arquivo2: String;
  Conteudo, Conteudo2, Extract, Extract2: TStringList;
  i : Integer;
  ListaAuxiliar : TList<TBoss>;
  j: Integer;
begin
  Conteudo  := TStringList.Create();
  Conteudo2 := TStringList.Create();
  Extract   := TStringList.Create();
  Extract2  := TStringList.Create();
  Arquivo   := ExtractFilePath(Application.ExeName) + 'Bosses.txt';
  Arquivo2  := ExtractFilePath(Application.ExeName) + 'BossesImages.txt';

  if not FileExists(Arquivo) then
  begin
    ShowMessage('Arquivo dos boss n�o encontrado!');
    Application.Terminate;
  end;

  if not FileExists(Arquivo2) then
  begin
    ShowMessage('Arquivo de imagem dos boss n�o encontrado!');
    Application.Terminate;
  end;

  Conteudo.LoadFromFile(Arquivo);
  Conteudo2.LoadFromFile(Arquivo2);
  SetLength(Result, Conteudo.Count);

  if Conteudo.Count <> Conteudo2.Count then
  begin
    ShowMessage('Ocorreu uma diverg�ncia entre a quantidade de boss e a quantidade de imagens!' + #13 +
                'Favor verificar os arquivos Bosses.txt e BossesImages.txt');
    Application.Terminate;
  end;

  Extract.Delimiter := ',';
  Extract2.Delimiter := ',';

  for i := 0 to Conteudo.Count - 1 do
  begin
    ListaAuxiliar := TList<TBoss>.Create();
    Extract.DelimitedText  := Conteudo[i];
    Extract2.DelimitedText := Conteudo2[i];

    for j := 0 to Extract.Count - 1 do
    begin
      ListaAuxiliar.Add(TBoss.Create(Extract[j], Extract2[j], GetTime(j)));
    end;

    Result[i] := ListaAuxiliar;
  end;
end;

procedure LerConfiguracoes();
var
  ArquivoINI: TIniFile;
  Aux: String;
begin
  ArquivoINI := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Configuracao.ini');

  Aux := ArquivoINI.ReadString('Configuracoes', 'TempoEspera', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'TempoEspera', '0');
    Configuracao.TempoEspera := 0;
  end
  else
    Configuracao.TempoEspera := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'Antecedencia', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'Antecedencia', '15');
    Configuracao.Antecedencia := 15;
  end
  else
    Configuracao.Antecedencia := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'Intervalo', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'Intervalo', '3');
    Configuracao.Intervalo := 3;
  end
  else
    Configuracao.Intervalo := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'Piscar', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'Piscar', '4');
    Configuracao.Piscar := 4;
  end
  else
    Configuracao.Piscar := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'ExibeTimer', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'ExibeTimer', '1');
    Configuracao.ExibeTimer := true;
  end
  else
    Configuracao.ExibeTimer := StrToBool(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'ExibeNotificacao', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'ExibeNotificacao', '1');
    Configuracao.ExibeNotificacao := true;
  end
  else
    Configuracao.ExibeNotificacao := StrToBool(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'Blend', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'Blend', '255');
    Configuracao.Blend := 255;
  end
  else
    Configuracao.Blend := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'Tamanho', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'Tamanho', '100');
    Configuracao.Tamanho := 100;
  end
  else
    Configuracao.Tamanho := StrToInt(Aux);

  if FileExists(ExtractFilePath(Application.ExeName) + 'FontBoss.font') then
  begin
    Configuracao.FonteBoss := TFont.Create();
    LoadFontFromStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBoss.font', fmOpenRead), Configuracao.FonteBoss);
  end;

  if FileExists(ExtractFilePath(Application.ExeName) + 'FontBossTime.font') then
  begin
    Configuracao.FonteBossTime := TFont.Create();
    LoadFontFromStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBossTime.font', fmOpenRead), Configuracao.FonteBossTime);
  end;

  if FileExists(ExtractFilePath(Application.ExeName) + 'FontFoodTime.font') then
  begin
    Configuracao.FonteFoodTime := TFont.Create();
    LoadFontFromStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontFoodTime.font', fmOpenRead), Configuracao.FonteFoodTime);
  end;

  Aux := ArquivoINI.ReadString('Configuracoes', 'AudioFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'AudioFood', '0');
    Configuracao.AudioFood := 0;
  end
  else
    Configuracao.AudioFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'ExibeTimerFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'ExibeTimerFood', '0');
    Configuracao.ExibeTimerFood := true;
  end
  else
    Configuracao.ExibeTimerFood := StrToBool(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'TempoEsperaFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'TempoEsperaFood', '100');
    Configuracao.TempoEsperaFood := 0;
  end
  else
    Configuracao.TempoEsperaFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'AntecedenciaFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'AntecedenciaFood', '15');
    Configuracao.AntecedenciaFood := 15;
  end
  else
    Configuracao.AntecedenciaFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'IntervaloFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'IntervaloFood', '3');
    Configuracao.IntervaloFood := 3;
  end
  else
    Configuracao.IntervaloFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'PiscarFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'PiscarFood', '4');
    Configuracao.PiscarFood := 4;
  end
  else
    Configuracao.PiscarFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'BlendFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'BlendFood', '255');
    Configuracao.BlendFood := 255;
  end
  else
    Configuracao.BlendFood := StrToInt(Aux);

  Aux := ArquivoINI.ReadString('Configuracoes', 'TamanhoFood', '');
  if (Trim(Aux) = '') then
  begin
    ArquivoINI.WriteString('Configuracoes', 'TamanhoFood', '100');
    Configuracao.TamanhoFood := 100;
  end
  else
    Configuracao.TamanhoFood := StrToInt(Aux);

  ArquivoINI.Free;
end;

procedure SaveFontToStream(AStream: TFileStream; AFont: TFont);
var LogFont: TLogFont;
    Color: TColor;
begin
  if GetObject(AFont.Handle, SizeOf(LogFont), @LogFont) = 0 then
    RaiseLastOSError;
  AStream.WriteBuffer(LogFont, SizeOf(LogFont));
  Color := AFont.Color;
  AStream.WriteBuffer(Color, SizeOf(Color));
  AStream.Free;
end;

procedure LoadFontFromStream(AStream: TFileStream; var AFont: TFont);
var LogFont: TLogFont;
    F: HFONT;
    Color: TColor;
begin
  AStream.ReadBuffer(LogFont, SizeOf(LogFont));
  F := CreateFontIndirect(LogFont);
  if F = 0 then
    RaiseLastOSError;
  AFont.Handle := F;
  AStream.ReadBuffer(Color, SizeOf(Color));
  AFont.Color := Color;
  AStream.Free;
end;

procedure SalvarConfiguracao(Configuracao, Valor: String);
var
  ArquivoINI: TIniFile;
begin
  ArquivoINI := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Configuracao.ini');
  ArquivoINI.WriteString('Configuracoes', Configuracao, Valor);
  ArquivoINI.Free;
end;

function GetTime(Indice: Integer): TTime;
begin
  case Indice of
    0: result := StrToTime('02:00:00');
    1: result := StrToTime('11:00:00');
    2: result := StrToTime('16:00:00');
    3: result := StrToTime('18:00:00');
    4: result := StrToTime('20:00:00');
    5: result := StrToTime('23:30:00');
  end;
end;

function GetNowDayWeek(): Integer;
var
  Atual: TDateTime;
begin
  Atual := IncMinute(Now, Configuracao.TempoEspera);
  if (Atual > IncMinute(StrToDateTime(DateToStr(Date) + ' 23:30:00'), Configuracao.TempoEspera)) then
  begin
    result := DayOfWeek(IncDay(Date, 1));
  end
  else
    result := DayOfWeek(Date);

  if ((DayOfWeek(Atual) = 7) and (Atual > IncMinute(StrToDateTime(DateToStr(Date) + ' 20:00:00'), Configuracao.TempoEspera))) then
  begin
    Result := 1;
  end;
end;

function GetNowTime(): Integer;
var
  Atual: TDateTime;
  DataString: String;
  myYear, myMonth, myDay : Word;
begin
  Atual := IncMinute(Now, Configuracao.TempoEspera * -1);
  if (Atual > StrToDateTime(DateToStr(Date) + ' 23:30:00')) then
  begin
    DataString := DateToStr(IncDay(Date, 1));
    Atual := StrToDateTime(DataString + ' 00:00:00');
  end
  else
    DataString := DateToStr(Date);

  DecodeDate(Atual, myYear, myMonth, myDay);

  if (Atual < EncodeDateTime(myYear, myMonth, myDay, 23, 0, 0, 0)) then
    Result := 5;
  if (Atual < EncodeDateTime(myYear, myMonth, myDay, 20, 0, 0, 0)) then
    Result := 4;
  if (CompareDateTime(Atual, EncodeDateTime(myYear, myMonth, myDay, 18, 0, 0, 0)) < 0) then
    if (DayOfWeek(Atual) = 1) then
    Result := 3;
  if (Atual < EncodeDateTime(myYear, myMonth, myDay, 16, 0, 0, 0)) then
    Result := 2;
  if (Atual < EncodeDateTime(myYear, myMonth, myDay, 11, 0, 0, 0)) then
    Result := 1;
  if (Atual < EncodeDateTime(myYear, myMonth, myDay, 02, 0, 0, 0)) then
    Result := 0;

  if ((DayOfWeek(Atual) = 7) and (Result in [5])) then
  begin
    DataString := DateToStr(IncDay(Date, 1));
    Atual := StrToDateTime(DataString + ' 00:00:00');
    Result := 0;
  end;
end;

Function ReturnTimeInternet(const Servidor: string): TDateTime; //atualiza��es futuras; obter horario da internet
Var
  SNTP: TIdSNTP;
begin
  SNTP := TIdSNTP.Create(nil);
  try
    SNTP.Host := Servidor;
    Result := SNTP.DateTime;
  finally
    SNTP.Disconnect;
    SNTP.Free;
  end;
end;

end.
