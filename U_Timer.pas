unit U_Timer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, U_Funcoes;

type
  TF_Timer = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    Image2: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormMouseEnter(Sender: TObject);
  private
    { Private declarations }
  public
    procedure GerarImagensCirculo();
    procedure ResizeAll(Valor: Integer);
    { Public declarations }
  end;

var
  F_Timer: TF_Timer;

implementation

uses
  UTaskBarList, U_FoodTimer;

{$R *.dfm}

procedure TF_Timer.FormCreate(Sender: TObject);
begin
  Self.FormStyle := fsStayOnTop;
  parentWindow := getDesktopWindow;
  F_FoodTimer := TF_FoodTimer.Create(Self);
  SetWindowLong(F_FoodTimer.Handle, GWL_EXSTYLE,
  GetWindowLong(F_FoodTimer.Handle, GWL_EXSTYLE)
                or WS_EX_TOOLWINDOW);
end;

procedure TF_Timer.FormMouseEnter(Sender: TObject);
begin
  if (Configuracao.BloquearClick) then
  begin
    Self.Hide;
    Configuracao.MouseIn := True;
  end;
end;

procedure TF_Timer.ResizeAll(Valor: Integer);
begin
  F_Timer.Label1.Width := Trunc(317 * (Valor/100));
  F_Timer.Label1.Height := Trunc(25 * (Valor/100));
  F_Timer.Label1.Top := Trunc(83 * (Valor/100));
  F_Timer.Label1.Font.Size := Trunc(Configuracao.FonteBoss.Size * (Valor/100));

  F_Timer.Label2.Width := Trunc(317 * (Valor/100));
  F_Timer.Label2.Height := Trunc(25 * (Valor/100));
  F_Timer.Label2.Top := Trunc(112 * (Valor/100));
  F_Timer.Label2.Font.Size := Trunc(Configuracao.FonteBossTime.Size * (Valor/100));

  F_Timer.Panel1.Width := Trunc(81 * (Valor/100));
  F_Timer.Panel1.Height := Trunc(78 * (Valor/100));
  if F_Timer.Panel2.Visible then
    F_Timer.Panel1.Left := Trunc(88 * (Valor/100))
  else
    F_Timer.Panel1.Left := Trunc(136 * (Valor/100));

  F_Timer.Panel2.Width := Trunc(81 * (Valor/100));
  F_Timer.Panel2.Height := Trunc(78 * (Valor/100));
  F_Timer.Panel2.Left := Trunc(175 * (Valor/100));

  F_Timer.GerarImagensCirculo;
end;

procedure TF_Timer.GerarImagensCirculo();
var
  rgn: HRGN;
  dc : HDC;
begin
  rgn := CreateEllipticRgn(0, 0, Image1.Width, Image1.Height);
  dc := GetDC(Panel1.Handle);
  SetWindowRgn(Panel1.Handle, rgn, true);
  ReleaseDC(Panel1.Handle, dc);
  DeleteObject(rgn);

  rgn := CreateEllipticRgn(0, 0, Image2.Width, Image2.Height);
  dc := GetDC(Panel2.Handle);
  SetWindowRgn(Panel2.Handle, rgn, true);
  ReleaseDC(Panel2.Handle, dc);
  DeleteObject(rgn);
end;

procedure TF_Timer.FormShow(Sender: TObject);
begin
  Self.AlphaBlendValue := Configuracao.Blend;

  if Assigned(Configuracao.FonteBoss) then
    Self.Label1.Font := Configuracao.FonteBoss
  else
  begin
    Configuracao.FonteBoss := Self.Label1.Font;
    SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBoss.font', fmCreate), Self.Label1.Font);
  end;

  if Assigned(Configuracao.FonteBossTime) then
    Self.Label2.Font := Configuracao.FonteBossTime
  else
  begin
    Configuracao.FonteBossTime := Self.Label2.Font;
    SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontBossTime.font', fmCreate), Self.Label2.Font);
  end;

  ResizeAll(Configuracao.Tamanho);
  TTaskbarList.Remove(Self.Handle);

  self.TransparentColorValue := self.Color;
  self.TransparentColor := True;

  F_Timer.Repaint;
end;

procedure TF_Timer.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

end.
