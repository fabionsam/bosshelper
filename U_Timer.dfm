object F_Timer: TF_Timer
  Left = 0
  Top = 0
  AlphaBlend = True
  BorderStyle = bsNone
  ClientHeight = 140
  ClientWidth = 325
  Color = clBackground
  TransparentColor = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnMouseEnter = FormMouseEnter
  OnShow = FormShow
  DesignSize = (
    325
    140)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 83
    Width = 314
    Height = 23
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Label1MouseDown
    OnMouseEnter = FormMouseEnter
  end
  object Label2: TLabel
    Left = 3
    Top = 112
    Width = 317
    Height = 25
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
    OnMouseDown = Label1MouseDown
    OnMouseEnter = FormMouseEnter
  end
  object Panel1: TPanel
    Left = 78
    Top = 2
    Width = 81
    Height = 78
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Panel1'
    TabOrder = 0
    OnMouseDown = Label1MouseDown
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 79
      Height = 76
      Align = alClient
      AutoSize = True
      Stretch = True
      OnMouseDown = Label1MouseDown
      OnMouseEnter = FormMouseEnter
      ExplicitLeft = 2
      ExplicitTop = -1
    end
  end
  object Panel2: TPanel
    Left = 162
    Top = 2
    Width = 81
    Height = 78
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Panel1'
    TabOrder = 1
    Visible = False
    OnMouseDown = Label1MouseDown
    object Image2: TImage
      Left = 1
      Top = 1
      Width = 79
      Height = 76
      Align = alClient
      AutoSize = True
      Stretch = True
      OnMouseDown = Label1MouseDown
      OnMouseEnter = FormMouseEnter
      ExplicitLeft = 0
    end
  end
end
