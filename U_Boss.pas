unit U_Boss;

interface

type
  TBoss = record
    Nome: String;
    Horario: TTime;
    ImageName: String;

    constructor Create(Nome, Imagem: string; Horario: TTime);
  end;

implementation

{ TBoss }

constructor TBoss.Create(Nome, Imagem: string; Horario: TTime);
begin
  Self.Nome := Nome;
  Self.Horario := Horario;
  Self.ImageName := Imagem;
end;


end.
