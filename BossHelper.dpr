program BossHelper;

uses
  Vcl.Forms,
  U_Principal in 'U_Principal.pas' {Form1},
  U_Timer in 'U_Timer.pas' {F_Timer},
  U_Funcoes in 'U_Funcoes.pas',
  Windows,
  U_Boss in 'U_Boss.pas',
  UTaskBarList in 'UTaskBarList.pas',
  U_FoodTimer in 'U_FoodTimer.pas' {F_FoodTimer};

{$R *.res}

begin
  Application.Initialize;
  Application.ShowMainForm := False;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TF_Timer, F_Timer);
  //Application.CreateForm(TF_FoodTimer, F_FoodTimer);
  Application.Run;
end.
