object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Configura'#231#245'es'
  ClientHeight = 331
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 265
    Height = 289
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Boss Timer'
      object Label1: TLabel
        Left = 11
        Top = 91
        Width = 149
        Height = 13
        Caption = 'Manter aviso do boss por mais:'
      end
      object Label2: TLabel
        Left = 186
        Top = 91
        Width = 49
        Height = 13
        Caption = 'minuto(s).'
      end
      object Label3: TLabel
        Left = 11
        Top = 122
        Width = 121
        Height = 13
        Caption = 'Fonte do nome dos boss:'
      end
      object Label4: TLabel
        Left = 11
        Top = 153
        Width = 125
        Height = 13
        Caption = 'Fonte do tempo dos boss:'
      end
      object Label5: TLabel
        Left = 11
        Top = 184
        Width = 72
        Height = 13
        Caption = 'Transpar'#234'ncia:'
      end
      object Label8: TLabel
        Left = 11
        Top = 37
        Width = 134
        Height = 13
        Caption = 'notificar novamente a cada '
      end
      object Label9: TLabel
        Left = 174
        Top = 37
        Width = 49
        Height = 13
        Caption = 'minuto(s).'
      end
      object Label10: TLabel
        Left = 11
        Top = 64
        Width = 80
        Height = 13
        Caption = 'Piscar timer com '
      end
      object Label11: TLabel
        Left = 119
        Top = 64
        Width = 131
        Height = 13
        Caption = 'minuto(s) de anteced'#234'ncia.'
      end
      object Label12: TLabel
        Left = 11
        Top = 226
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label6: TLabel
        Left = 11
        Top = 13
        Width = 65
        Height = 13
        Caption = 'Notificar com '
      end
      object Label7: TLabel
        Left = 102
        Top = 13
        Width = 136
        Height = 13
        Caption = 'minuto(s) de anteced'#234'ncia e'
      end
      object Edit1: TEdit
        Left = 166
        Top = 88
        Width = 14
        Height = 21
        TabOrder = 0
        Text = '0'
        OnExit = Edit1Exit
      end
      object Button3: TButton
        Left = 138
        Top = 117
        Width = 75
        Height = 25
        Caption = 'Configurar'
        TabOrder = 1
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 138
        Top = 148
        Width = 75
        Height = 25
        Caption = 'Configurar'
        TabOrder = 2
        OnClick = Button4Click
      end
      object TrackBar1: TTrackBar
        Left = 89
        Top = 179
        Width = 150
        Height = 36
        Max = 255
        Position = 255
        TabOrder = 3
        ThumbLength = 25
        OnChange = TrackBar1Change
      end
      object Edit3: TEdit
        Left = 146
        Top = 34
        Width = 22
        Height = 21
        Hint = 'Mantenha zero para n'#227'o notificar novamente.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Text = '3'
        OnExit = Edit3Exit
      end
      object Edit4: TEdit
        Left = 93
        Top = 61
        Width = 22
        Height = 21
        Hint = 'Mantenha zero para n'#227'o piscar.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Text = '4'
        OnExit = Edit4Exit
      end
      object TrackBar2: TTrackBar
        Left = 89
        Top = 221
        Width = 150
        Height = 36
        Max = 100
        Position = 100
        TabOrder = 6
        ThumbLength = 25
        OnChange = TrackBar2Change
      end
      object Edit2: TEdit
        Left = 76
        Top = 10
        Width = 22
        Height = 21
        TabOrder = 7
        Text = '15'
        OnExit = Edit2Exit
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Food Timer'
      ImageIndex = 1
      object Label13: TLabel
        Left = 11
        Top = 5
        Width = 65
        Height = 13
        Caption = 'Notificar com '
      end
      object Label14: TLabel
        Left = 102
        Top = 5
        Width = 136
        Height = 13
        Caption = 'minuto(s) de anteced'#234'ncia e'
      end
      object Label15: TLabel
        Left = 11
        Top = 29
        Width = 134
        Height = 13
        Caption = 'notificar novamente a cada '
      end
      object Label16: TLabel
        Left = 174
        Top = 29
        Width = 49
        Height = 13
        Caption = 'minuto(s).'
      end
      object Label17: TLabel
        Left = 11
        Top = 56
        Width = 80
        Height = 13
        Caption = 'Piscar timer com '
      end
      object Label18: TLabel
        Left = 118
        Top = 56
        Width = 131
        Height = 13
        Caption = 'minuto(s) de anteced'#234'ncia.'
      end
      object Label21: TLabel
        Left = 10
        Top = 85
        Width = 72
        Height = 13
        Caption = 'Transpar'#234'ncia:'
      end
      object Label22: TLabel
        Left = 10
        Top = 127
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label23: TLabel
        Left = 11
        Top = 165
        Width = 74
        Height = 13
        Caption = 'Fonte do timer:'
      end
      object Edit5: TEdit
        Left = 76
        Top = 2
        Width = 22
        Height = 21
        TabOrder = 0
        Text = '15'
        OnExit = Edit2Exit
      end
      object Edit6: TEdit
        Left = 146
        Top = 26
        Width = 22
        Height = 21
        Hint = 'Mantenha zero para n'#227'o notificar novamente.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '3'
        OnExit = Edit3Exit
      end
      object Edit7: TEdit
        Left = 93
        Top = 53
        Width = 22
        Height = 21
        Hint = 'Mantenha zero para n'#227'o piscar.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = '4'
        OnExit = Edit4Exit
      end
      object TrackBar3: TTrackBar
        Left = 88
        Top = 80
        Width = 150
        Height = 36
        Max = 255
        Position = 255
        TabOrder = 3
        ThumbLength = 25
        OnChange = TrackBar3Change
      end
      object TrackBar4: TTrackBar
        Left = 88
        Top = 118
        Width = 150
        Height = 36
        Enabled = False
        Max = 100
        Position = 100
        TabOrder = 4
        ThumbLength = 25
        OnChange = TrackBar2Change
      end
      object Button5: TButton
        Left = 91
        Top = 160
        Width = 75
        Height = 25
        Caption = 'Configurar'
        TabOrder = 5
        OnClick = Button5Click
      end
      object RadioGroup1: TRadioGroup
        Left = 11
        Top = 191
        Width = 227
        Height = 38
        Caption = 'Audio'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Bip'
          'Comendo'
          'Ratinho')
        TabOrder = 6
      end
    end
  end
  object Button2: TButton
    Left = 160
    Top = 303
    Width = 53
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button1: TButton
    Left = 219
    Top = 303
    Width = 54
    Height = 25
    Caption = 'Salvar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object TrayIcon1: TTrayIcon
    PopupMenu = PopupMenu1
    Left = 32
    Top = 295
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 8
    Top = 295
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 96
    Top = 295
  end
  object FontDialog2: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 128
    Top = 295
  end
  object PopupMenu1: TPopupMenu
    Left = 64
    Top = 290
    object MostrarRelgiodeBoss1: TMenuItem
      Caption = 'Mostrar Rel'#243'gio de Boss'
      OnClick = MostrarRelgiodeBoss1Click
    end
    object MostrarRelgiodeComida1: TMenuItem
      Caption = 'Mostrar Rel'#243'gio de Comida'
      OnClick = MostrarRelgiodeComida1Click
    end
    object BloquearClicknoRelgio1: TMenuItem
      Caption = 'Esconder rel'#243'gio ao passar o mouse'
      Hint = 'Evita cliques indesejados.'
      OnClick = BloquearClicknoRelgio1Click
    end
    object MostrarNotificao1: TMenuItem
      Caption = 'Mostrar Notifica'#231#227'o'
      OnClick = MostrarNotificao1Click
    end
    object Configuraes1: TMenuItem
      Caption = 'Configura'#231#227'o'
      OnClick = Configuraes1Click
    end
    object Fechar1: TMenuItem
      Caption = 'Fechar'
      OnClick = Fechar1Click
    end
  end
  object FontDialog3: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 160
    Top = 295
  end
end
