unit U_FoodTimer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ImgList, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TF_FoodTimer = class(TForm)
    Timer1: TTimer;
    ImageList1: TImageList;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label1: TLabel;
    SpeedButton4: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure Label1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormMouseEnter(Sender: TObject);
  private
    { Private declarations }
    Pause : Boolean;
    NotificouFood : Boolean;
    function GetAudio: String;
  public
    { Public declarations }
    FoodTime : TTime;
    LastNotificationFood : TTime;
  end;

var
  F_FoodTimer: TF_FoodTimer;

implementation

uses
  System.DateUtils, U_Funcoes, MMSystem, U_Principal;

{$R *.dfm}

procedure TF_FoodTimer.FormCreate(Sender: TObject);
begin
  Self.FormStyle := fsStayOnTop;
  parentWindow := getDesktopWindow;
  Pause := False;

  FoodTime := 0;
  FoodTime := IncMinute(FoodTime, 30);
  Label1.Caption := FormatDateTime('nn:ss', FoodTime);

  ImageList1.GetBitmap(1, SpeedButton1.Glyph);
  ImageList1.GetBitmap(3, SpeedButton2.Glyph);
  ImageList1.GetBitmap(5, SpeedButton3.Glyph);
  ImageList1.GetBitmap(6, SpeedButton4.Glyph);
end;

procedure TF_FoodTimer.FormMouseEnter(Sender: TObject);
begin
  if (Configuracao.BloquearClick) then
  begin
    Self.Hide;
    Configuracao.MouseInFood := True;
  end;
end;

procedure TF_FoodTimer.FormShow(Sender: TObject);
begin
  Self.AlphaBlendValue := Configuracao.BlendFood;

  if Assigned(Configuracao.FonteFoodTime) then
    Self.Label1.Font := Configuracao.FonteFoodTime
  else
  begin
    Configuracao.FonteFoodTime := Self.Label1.Font;
    SaveFontToStream(TFileStream.Create(ExtractFilePath(Application.ExeName) + 'FontFoodTime.font', fmCreate), Self.Label1.Font);
  end;
end;

procedure TF_FoodTimer.Label1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TF_FoodTimer.SpeedButton1Click(Sender: TObject);
begin
  SpeedButton1.Enabled := False;
  SpeedButton2.Enabled := True;
  SpeedButton3.Enabled := True;
  if not(Pause) then
  begin
    FoodTime := 0;
    FoodTime := IncMinute(FoodTime, 30);
  end;
  Timer1.Enabled := True;
end;

procedure TF_FoodTimer.SpeedButton2Click(Sender: TObject);
begin
  SpeedButton1.Enabled := True;
  SpeedButton2.Enabled := False;
  SpeedButton3.Enabled := False;
  Timer1.Enabled := False;
  Pause := False;
  FoodTime := 0;
  FoodTime := IncMinute(FoodTime, 30);
  Label1.Caption := FormatDateTime('nn:ss', FoodTime);
end;

procedure TF_FoodTimer.SpeedButton3Click(Sender: TObject);
begin
  SpeedButton1.Enabled := True;
  SpeedButton2.Enabled := False;
  SpeedButton3.Enabled := False;
  Timer1.Enabled := False;
  Pause := True;
end;

procedure TF_FoodTimer.Timer1Timer(Sender: TObject);
var
  myHour, myMin, mySec, myMilli : Word;
begin
  FoodTime := IncSecond(FoodTime, -1);
  Label1.Caption := FormatDateTime('nn:ss', FoodTime);

  DecodeTime(FoodTime, myHour, myMin, mySec, myMilli);
  if (Form1.MostrarNotificao1.Checked) and (not NotificouFood) and (myMin <= Configuracao.AntecedenciaFood) then
  begin
    NotificouFood := True;
    LastNotificationFood := Time;
    Form1.TrayIcon1.BalloonHint := 'Faltam ' + IntToStr(myMin) + ' minutos para a pr�xima refei��o';
    Form1.TrayIcon1.ShowBalloonHint;
    sndPlaySound(PChar(ExtractFilePath(Application.ExeName) + GetAudio()), SND_ASYNC);
  end;

  if (Configuracao.IntervaloFood > 0) and (Form1.MostrarNotificao1.Checked) and (NotificouFood) and (MinutesBetween(Time, LastNotificationFood) >= Configuracao.IntervaloFood) then
  begin
    LastNotificationFood := Time;
    Form1.TrayIcon1.BalloonHint := 'Faltam ' + IntToStr(myMin) + ' minutos para a pr�xima refei��o';
    Form1.TrayIcon1.ShowBalloonHint;
    sndPlaySound(PChar(ExtractFilePath(Application.ExeName) + GetAudio()), SND_ASYNC);
  end;

  if (Form1.MostrarRelgiodeComida1.Checked) then
  begin
    if (Configuracao.PiscarFood > 0) and (myMin <= Configuracao.PiscarFood) then
    begin
      if (Label1.Visible) then
        Label1.Hide
      else
        Label1.Show;
    end
    else
      if (not Label1.Visible) then
        Label1.Show;
  end;

  if (FoodTime <= 0) then
  begin
    FoodTime := 0;
    FoodTime := IncMinute(FoodTime, 30);
  end;
end;

function TF_FoodTimer.GetAudio(): String;
begin
  case Configuracao.AudioFood of
    0 : result := 'sound\bip.wav';
    1 : result := 'sound\eat.wav';
    2 : result := 'sound\squish.wav';
  end;
end;

end.
